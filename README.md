# README #

--

[1]: http://adventofcode.com/2016
[2]: http://adventofcode.com/2016/day/1
[3]: http://adventofcode.com/2016/day/1/input

Complete a challenge featured on [Advent of Code][1]
This Javascript app that fetches the [input][3] and returns a solution to the challenge.
* Authenticate with your GitHub/Google/Twitter/Reddit account [Advent of Code][2]

## Build

### Prerequisites
* Node.js and NPM
* gulp NPM module

### First Build
* Execute `npm install`
* Execute `gulp` which will perform 'build' and starts 'watch' task.

---

## Configuration


---

## TODO
