(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
// follow @HenrikJoreteg and @andyet if you like this ;)
(function () {
    var inNode = typeof window === 'undefined',
        ls = !inNode && window.localStorage,
        out = {};

    if (inNode) {
        module.exports = console;
        return;
    }

    var andlogKey = ls.andlogKey || 'debug'
    if (ls && ls[andlogKey] && window.console) {
        out = window.console;
    } else {
        var methods = "assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,markTimeline,profile,profileEnd,time,timeEnd,trace,warn".split(","),
            l = methods.length,
            fn = function () {};

        while (l--) {
            out[methods[l]] = fn;
        }
    }
    if (typeof exports !== 'undefined') {
        module.exports = out;
    } else {
        window.console = out;
    }
})();

},{}],2:[function(require,module,exports){
(function (process){
(function() {
  function checkColorSupport() {
    if (typeof window === 'undefined' || typeof navigator === 'undefined') {
      return false;
    }

    var chrome = !!window.chrome,
        firefox = /firefox/i.test(navigator.userAgent),
        firefoxVersion,
        electron = process && process.versions && process.versions.electron;

    if (firefox) {
        var match = navigator.userAgent.match(/Firefox\/(\d+\.\d+)/);
        if (match && match[1] && Number(match[1])) {
            firefoxVersion = Number(match[1]);
        }
    }
    return chrome || firefoxVersion >= 31.0 || electron;
  }

  var yieldColor = function() {
    var goldenRatio = 0.618033988749895;
    hue += goldenRatio;
    hue = hue % 1;
    return hue * 360;
  };

  var inNode = typeof window === 'undefined',
      ls = !inNode && window.localStorage,
      debugKey = ls.andlogKey || 'debug',
      debug = ls[debugKey],
      logger = require('andlog'),
      bind = Function.prototype.bind,
      hue = 0,
      padding = true,
      separator = '|',
      padLength = 15,
      noop = function() {},
      // if ls.debugColors is set, use that, otherwise check for support
      colorsSupported = ls.debugColors ? (ls.debugColors !== "false") : checkColorSupport(),
      bows = null,
      debugRegex = null,
      invertRegex = false,
      moduleColorsMap = {};

  if (debug && debug[0] === '!' && debug[1] === '/') {
    invertRegex = true;
    debug = debug.slice(1);
  }
  debugRegex = debug && debug[0]==='/' && new RegExp(debug.substring(1,debug.length-1));

  var logLevels = ['log', 'debug', 'warn', 'error', 'info'];

  //Noop should noop
  for (var i = 0, ii = logLevels.length; i < ii; i++) {
      noop[ logLevels[i] ] = noop;
  }

  bows = function(str) {
    var msg, colorString, logfn;

    if (padding) {
      msg = (str.slice(0, padLength));
      msg += Array(padLength + 3 - msg.length).join(' ') + separator;
    } else {
      msg = str + Array(3).join(' ') + separator;
    }

    if (debugRegex) {
        var matches = str.match(debugRegex);
        if (
            (!invertRegex && !matches) ||
            (invertRegex && matches)
        ) return noop;
    }

    if (!bind) return noop;

    var logArgs = [logger];
    if (colorsSupported) {
      if(!moduleColorsMap[str]){
        moduleColorsMap[str]= yieldColor();
      }
      var color = moduleColorsMap[str];
      msg = "%c" + msg;
      colorString = "color: hsl(" + (color) + ",99%,40%); font-weight: bold";

      logArgs.push(msg, colorString);
    }else{
      logArgs.push(msg);
    }

    if(arguments.length>1){
        var args = Array.prototype.slice.call(arguments, 1);
        logArgs = logArgs.concat(args);
    }

    logfn = bind.apply(logger.log, logArgs);

    logLevels.forEach(function (f) {
      logfn[f] = bind.apply(logger[f] || logfn, logArgs);
    });
    return logfn;
  };

  bows.config = function(config) {
    if (config.padLength) {
      padLength = config.padLength;
    }

    if (typeof config.padding === 'boolean') {
      padding = config.padding;
    }

    if (config.separator) {
      separator = config.separator;
    } else if (config.separator === false || config.separator === '') {
      separator = ''
    }
  };

  if (typeof module !== 'undefined') {
    module.exports = bows;
  } else {
    window.bows = bows;
  }
}).call();

}).call(this,require("rH1JPG"))
},{"andlog":1,"rH1JPG":3}],3:[function(require,module,exports){
// shim for using process in browser

var process = module.exports = {};

process.nextTick = (function () {
    var canSetImmediate = typeof window !== 'undefined'
    && window.setImmediate;
    var canPost = typeof window !== 'undefined'
    && window.postMessage && window.addEventListener
    ;

    if (canSetImmediate) {
        return function (f) { return window.setImmediate(f) };
    }

    if (canPost) {
        var queue = [];
        window.addEventListener('message', function (ev) {
            var source = ev.source;
            if ((source === window || source === null) && ev.data === 'process-tick') {
                ev.stopPropagation();
                if (queue.length > 0) {
                    var fn = queue.shift();
                    fn();
                }
            }
        }, true);

        return function nextTick(fn) {
            queue.push(fn);
            window.postMessage('process-tick', '*');
        };
    }

    return function nextTick(fn) {
        setTimeout(fn, 0);
    };
})();

process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;

process.binding = function (name) {
    throw new Error('process.binding is not supported');
}

// TODO(shtylman)
process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};

},{}],4:[function(require,module,exports){

var log = require('bows')('App.Config');
var App = window.App || {};

App.Config = (function () {

	var localDataPath = '../data/';

    var init = function() {
    	log('init');
    };

    return {
        Init : init,

        GetLocalDataPath : function () {
        	return localDataPath;
        }
    };

}());
},{"bows":2}],5:[function(require,module,exports){

var log = require('bows')('App.Utils');
var App = window.App || {};

App.Utils = (function () {

    var init = function() {
    	log('init');
    };

    var getInputData = function (url, local, cb) {
        var path = (local) ? App.Config.GetLocalDataPath() : '';
        var request = new XMLHttpRequest();


        request.onload = function () {
            if (request.status >= 200 && request.status < 400) {
                var response = request.responseText;

                cb(response);

            } else {
                log('Server error');
            }
        };

        request.onerror = function () {
            log('Connection error');
        };

        request.open('GET', path + url, true);

        setTimeout(function(){
            request.send();
        }, 0);
    };

    return {
        Init : init,
        GetInputData : getInputData
    };

}());
},{"bows":2}],6:[function(require,module,exports){

var log = require('bows')('App.DayOne');
var App = window.App || {};

App.DayOne = (function () {

    var inputData = [],
        direction = [{
                dir: 'north',
                axis : 'y',
                val : 1,
                deg : 0
            }, {
                dir: 'east',
                axis : 'x',
                val : 1,
                deg : 90
            }, {
                dir: 'south',
                axis : 'y',
                val : -1,
                deg : 180
            }, {
                dir: 'west',
                axis : 'x',
                val : -1,
                deg : -90
            }
        ],
        pos = { 
            x : 0, 
            y : 0 
        },
        breadcrumb = [],
        currentDirIndex = 0, // starting direction is 'north', index 0 in direction array
        currentDegree = 0, // starting direction is 'north', rotation is 0
        blocks = 0,
        stepSize = 10,
        citySize = 4000,
        cityGridEl,
        santaEl;

    var init = function () {
    	log('init');

        cityGridEl = document.querySelector('#city-grid');
        cityGridEl.style.width = citySize + 'px';
        cityGridEl.style.height = citySize + 'px';

        santaEl = document.querySelector('#santa');

        createGrids();
        App.Utils.GetInputData('1.txt', true, onInputDataReady);
    };

    var onInputDataReady = function (data) {
        inputData = data.split(', ');

        inputData.forEach(function(d, i){
            var turn = d.substr(0, 1),
                steps = parseInt(d.substr(1));

            currentDirIndex = getDirectionIndex(turn);

            var cardinalObj = direction[currentDirIndex],
                s = steps;

            while(s > 0) {
                var prevPos = {};

                prevPos.x = pos.x;
                prevPos.y = pos.y;
                breadcrumb.push(prevPos);

                pos[cardinalObj.axis] += cardinalObj.val;

                s--;
            }

            createRoute(steps);

            //log(d, pos, cardinalObj.dir);
        });

        blocks = Math.abs(pos.x) + Math.abs(pos.y);

        // Move Santa to final position & facing the final direction
        santaEl.classList.add(direction[currentDirIndex].dir);
        santaEl.style.transform = 'rotate(' + direction[currentDirIndex].deg + 'deg)';
        santaEl.style.top = (pos.y * stepSize * -1) + 'px';
        santaEl.style.left = (pos.x * stepSize) + 'px';
        
        outputAnswer('[Day 1][Part 1] : ' + blocks + ' blocks away facing ' + direction[currentDirIndex].dir);
        findHqPos();
    };

    var getDirectionIndex = function(turn) {
        var dir = currentDirIndex;

        turn = turn.toUpperCase();

        if(turn === 'R') {
            dir += 1;
            dir = (dir > direction.length-1) ? 0 : dir;
        } 
        else if(turn === 'L'){
            dir -= 1;
            dir = (dir < 0) ? direction.length-1 : dir;
        }

        return dir;
    };

    var findRepeatPos = function(targetPos) {
        return breadcrumb.filter(function (o) {
                return targetPos.x === o.x && targetPos.y === o.y;
            });
    };

    var findHqPos = function() {

        var repeatPos;

        // Loop through each breadcrum to find 1st repeated position
        while(breadcrumb.length > 0) {
            var targetPos = breadcrumb.shift();

            repeatPos = findRepeatPos(targetPos);

            if (repeatPos.length > 0) {
                var hqBlock = Math.abs(repeatPos[0].x) + Math.abs(repeatPos[0].y),
                    hqEl = document.querySelector('#hq');

                outputAnswer('[Day 1][Part 2] : Easter Bunny HQ is ' + hqBlock + ' blocks away');
                hqEl.style.left = (repeatPos[0].x * stepSize) + 'px';
                hqEl.style.top = (repeatPos[0].y * stepSize * -1) + 'px';
                break;
            }
        }
    };

    var outputAnswer = function(answer) {
        var answerList = document.querySelector('.answers'),
            el = document.createElement('li');

        el.innerHTML = answer;
        answerList.appendChild(el);
    };

    var createGrids = function() {
        var numCol = citySize / stepSize,
            rowsEl = document.querySelector('#rows'),
            colsEl = document.querySelector('#cols');

        for(var i=0; i < numCol; i++) {
            var rowBlockEl = document.createElement('div');
            rowBlockEl.classList.add('block', 'row');
            rowBlockEl.style.top = (stepSize * i) + 'px';
            rowsEl.appendChild(rowBlockEl);

            var colBlockEl = document.createElement('div');
            colBlockEl.classList.add('block', 'col');
            colBlockEl.style.left = (stepSize * i) + 'px';
            colsEl.appendChild(colBlockEl);
        }
    };

    var createRoute = function(steps) {
        var stepsInPx = steps * stepSize,
            prevPos = breadcrumb[breadcrumb.length - steps],
            routeContainer = document.querySelector('#route'),
            lineEl = document.createElement('div');

        lineEl.classList.add('line');

        var startPos, lineHeight, lineWidth;

        if(prevPos.x - pos.x !== 0) {
            lineWidth = stepsInPx;
            lineHeight = 1;

            if(prevPos.x < pos.x) {
                startPos = prevPos;
            } else {
                startPos = pos;
            }
        } else if(prevPos.y - pos.y !== 0) {
            lineWidth = 1;
            lineHeight = stepsInPx;

            if(prevPos.y > pos.y) {
                startPos = prevPos;
            } else {
                startPos = pos;
            }
        }
        
        lineEl.style.top = (startPos.y * stepSize * -1) + 'px';
        lineEl.style.left = (startPos.x * stepSize) + 'px';
        lineEl.style.width = lineWidth + 'px';
        lineEl.style.height = lineHeight + 'px';

        routeContainer.appendChild(lineEl);
    };

    return {
        Init : init
    };

}());
},{"bows":2}],7:[function(require,module,exports){

var log = require('bows')('App.DayThree');
var App = window.App || {};

App.DayThree = (function () {

    var inputData = [];

    var init = function () {
    	log('init');


        App.Utils.GetInputData('3.txt', true, onInputDataReady);
    };

    var onInputDataReady = function(data) {
    	var dataArr = data.split('\n');

    	dataArr.forEach(function(codeStr, i){
    		var codeArr = codeStr.split('');

    		inputData.push(codeArr);
    	});

    	log(inputData);
    };

    var outputAnswer = function(answer) {
        var answerList = document.querySelector('.answers'),
            el = document.createElement('li');

        el.innerHTML = answer;
        answerList.appendChild(el);
    };

    return {
        Init : init
    };

}());

},{"bows":2}],8:[function(require,module,exports){

var log = require('bows')('App.DayTwo');
var App = window.App || {};

App.DayTwo = (function () {

    var inputData = [],
    	keypadMatrix = [
	//	col: 0, 1, 2
    		[1, 2, 3], //row 0
    		[4, 5, 6], //row 1
    		[7, 8, 9]  //row 2
    	],
    	altKeypadMatrix = [
	//	col:  0,  1,  2,  3, 4
    		[-1, -1, 1, -1, -1], 	 //row 0
    		[-1, 2, 3, 4, -1], 		 //row 1
    		[5, 6, 7, 8, 9], 		 //row 2
    		[-1, 'A', 'B', 'C', -1], //row 3
    		[-1, -1, 'D', -1, -1],   //row 4
    	],
    	currentPos = {},
    	outputPass = [],
    	startKeyNum = 5;

    var init = function () {
    	log('init');


        App.Utils.GetInputData('2.txt', true, onInputDataReady);
    };

    var onInputDataReady = function(data) {
    	var dataArr = data.split('\n');

    	// Get starting position before decode
    	getInitPos(keypadMatrix);

    	dataArr.forEach(function(codeStr, i){
    		var codeArr = codeStr.split(''),
    			outputDigit = -1;

    		inputData.push(codeArr); // store it just in case

    		codeArr.forEach(function(code, j){
    			decode(code, keypadMatrix);
    		});

    		outputDigit = keypadMatrix[currentPos.row][currentPos.col];
    		outputPass.push(outputDigit);
    	});

        outputAnswer('[Day 2][Part 1] : Bathroom passcode is ' + outputPass.join(''));

    	// Lets find out answer for Part 2
    	altKeypadDecode();
    };

    var getInitPos = function(keypad) {
    	var initPos = {};

    	// Search #5's position in the specified keypad matrix
    	keypad.forEach(function(keypadRow, r){
    		var c = keypadRow.indexOf(startKeyNum);

	    	if(c !== -1) {
	    		initPos.col = c;
	    		initPos.row = r;
	    	}
    	});

    	currentPos.col = initPos.col;
    	currentPos.row = initPos.row;
    };

    var altKeypadDecode = function() {
    	var outputDigit = -1;

    	// Empty outputPass array
    	outputPass.length = 0;

    	// Get starting position before decode
    	getInitPos(altKeypadMatrix);

    	inputData.forEach(function(codeArr, j){
			codeArr.forEach(function(code, j){
    			decode(code, altKeypadMatrix);
    		});

			outputDigit = altKeypadMatrix[currentPos.row][currentPos.col];
			outputPass.push(outputDigit);
		});

        outputAnswer('[Day 2][Part 2] : Real bathroom passcode is ' + outputPass.join(''));
    };

    var decode = function(code, keypad) {
    	var newPos = {row : currentPos.row, col : currentPos.col};

    	switch(code) {
    		case 'U':
    			newPos.row--;
    			newPos.row = (newPos.row < 0) ? 0 : newPos.row;
    		break;
    		case 'D':
    			newPos.row++;
    			newPos.row = (newPos.row > keypad.length-1) ? keypad.length-1 : newPos.row;
    		break;
    		case 'L':
    			newPos.col--;
    			newPos.col = (newPos.col < 0) ? 0 : newPos.col;
    		break;
    		case 'R':
    			newPos.col++;
    			newPos.col = (newPos.col > keypad[newPos.row].length-1) ? keypad[newPos.row].length-1 : newPos.col;
    		break;
    	}

    	// if new position lands on -1 of the keypad, it is an invalid position, hence don't move
		if(keypad[newPos.row][newPos.col] !== -1) {
			currentPos.row = newPos.row;
	    	currentPos.col = newPos.col;
	    }
    };

    var outputAnswer = function(answer) {
        var answerList = document.querySelector('.answers'),
            el = document.createElement('li');

        el.innerHTML = answer;
        answerList.appendChild(el);
    };

    return {
        Init : init
    };

}());
},{"bows":2}],9:[function(require,module,exports){

 // NOTE: Run "localStorage.debug=true" to have bows showing up in console.
var log = require('bows')('App');

var App = App || {};
window.App = App;

var Config = require('./Config');
var Utils = require('./Utils');
var DayOne = require('./adventofcode-2016/DayOne');
var DayTwo = require('./adventofcode-2016/DayTwo');
var DayThree = require('./adventofcode-2016/DayThree');

// App Initialization
// --------------------------------------------------
(function () {

	App.DayOne.Init();
	App.DayTwo.Init();
	App.DayThree.Init();

}());
},{"./Config":4,"./Utils":5,"./adventofcode-2016/DayOne":6,"./adventofcode-2016/DayThree":7,"./adventofcode-2016/DayTwo":8,"bows":2}]},{},[9])
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tZW93ZmlzaC9TaXRlcy93b3JrL3VzdHdvL2NpdHktZ3JpZC9ub2RlX21vZHVsZXMvYnJvd3Nlci1wYWNrL19wcmVsdWRlLmpzIiwiL1VzZXJzL21lb3dmaXNoL1NpdGVzL3dvcmsvdXN0d28vY2l0eS1ncmlkL25vZGVfbW9kdWxlcy9hbmRsb2cvYW5kbG9nLmpzIiwiL1VzZXJzL21lb3dmaXNoL1NpdGVzL3dvcmsvdXN0d28vY2l0eS1ncmlkL25vZGVfbW9kdWxlcy9ib3dzL2Jvd3MuanMiLCIvVXNlcnMvbWVvd2Zpc2gvU2l0ZXMvd29yay91c3R3by9jaXR5LWdyaWQvbm9kZV9tb2R1bGVzL3Byb2Nlc3MvYnJvd3Nlci5qcyIsIi9Vc2Vycy9tZW93ZmlzaC9TaXRlcy93b3JrL3VzdHdvL2NpdHktZ3JpZC9zcmMvc2NyaXB0cy9Db25maWcuanMiLCIvVXNlcnMvbWVvd2Zpc2gvU2l0ZXMvd29yay91c3R3by9jaXR5LWdyaWQvc3JjL3NjcmlwdHMvVXRpbHMuanMiLCIvVXNlcnMvbWVvd2Zpc2gvU2l0ZXMvd29yay91c3R3by9jaXR5LWdyaWQvc3JjL3NjcmlwdHMvYWR2ZW50b2Zjb2RlLTIwMTYvRGF5T25lLmpzIiwiL1VzZXJzL21lb3dmaXNoL1NpdGVzL3dvcmsvdXN0d28vY2l0eS1ncmlkL3NyYy9zY3JpcHRzL2FkdmVudG9mY29kZS0yMDE2L0RheVRocmVlLmpzIiwiL1VzZXJzL21lb3dmaXNoL1NpdGVzL3dvcmsvdXN0d28vY2l0eS1ncmlkL3NyYy9zY3JpcHRzL2FkdmVudG9mY29kZS0yMDE2L0RheVR3by5qcyIsIi9Vc2Vycy9tZW93ZmlzaC9TaXRlcy93b3JrL3VzdHdvL2NpdHktZ3JpZC9zcmMvc2NyaXB0cy9mYWtlXzUwNDJlYzNkLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNqSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDL0RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNwQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDMUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNqTkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN4Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3pJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt0aHJvdyBuZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpfXZhciBmPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChmLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGYsZi5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCIvLyBmb2xsb3cgQEhlbnJpa0pvcmV0ZWcgYW5kIEBhbmR5ZXQgaWYgeW91IGxpa2UgdGhpcyA7KVxuKGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgaW5Ob2RlID0gdHlwZW9mIHdpbmRvdyA9PT0gJ3VuZGVmaW5lZCcsXG4gICAgICAgIGxzID0gIWluTm9kZSAmJiB3aW5kb3cubG9jYWxTdG9yYWdlLFxuICAgICAgICBvdXQgPSB7fTtcblxuICAgIGlmIChpbk5vZGUpIHtcbiAgICAgICAgbW9kdWxlLmV4cG9ydHMgPSBjb25zb2xlO1xuICAgICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdmFyIGFuZGxvZ0tleSA9IGxzLmFuZGxvZ0tleSB8fCAnZGVidWcnXG4gICAgaWYgKGxzICYmIGxzW2FuZGxvZ0tleV0gJiYgd2luZG93LmNvbnNvbGUpIHtcbiAgICAgICAgb3V0ID0gd2luZG93LmNvbnNvbGU7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgdmFyIG1ldGhvZHMgPSBcImFzc2VydCxjb3VudCxkZWJ1ZyxkaXIsZGlyeG1sLGVycm9yLGV4Y2VwdGlvbixncm91cCxncm91cENvbGxhcHNlZCxncm91cEVuZCxpbmZvLGxvZyxtYXJrVGltZWxpbmUscHJvZmlsZSxwcm9maWxlRW5kLHRpbWUsdGltZUVuZCx0cmFjZSx3YXJuXCIuc3BsaXQoXCIsXCIpLFxuICAgICAgICAgICAgbCA9IG1ldGhvZHMubGVuZ3RoLFxuICAgICAgICAgICAgZm4gPSBmdW5jdGlvbiAoKSB7fTtcblxuICAgICAgICB3aGlsZSAobC0tKSB7XG4gICAgICAgICAgICBvdXRbbWV0aG9kc1tsXV0gPSBmbjtcbiAgICAgICAgfVxuICAgIH1cbiAgICBpZiAodHlwZW9mIGV4cG9ydHMgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIG1vZHVsZS5leHBvcnRzID0gb3V0O1xuICAgIH0gZWxzZSB7XG4gICAgICAgIHdpbmRvdy5jb25zb2xlID0gb3V0O1xuICAgIH1cbn0pKCk7XG4iLCIoZnVuY3Rpb24gKHByb2Nlc3Mpe1xuKGZ1bmN0aW9uKCkge1xuICBmdW5jdGlvbiBjaGVja0NvbG9yU3VwcG9ydCgpIHtcbiAgICBpZiAodHlwZW9mIHdpbmRvdyA9PT0gJ3VuZGVmaW5lZCcgfHwgdHlwZW9mIG5hdmlnYXRvciA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICB2YXIgY2hyb21lID0gISF3aW5kb3cuY2hyb21lLFxuICAgICAgICBmaXJlZm94ID0gL2ZpcmVmb3gvaS50ZXN0KG5hdmlnYXRvci51c2VyQWdlbnQpLFxuICAgICAgICBmaXJlZm94VmVyc2lvbixcbiAgICAgICAgZWxlY3Ryb24gPSBwcm9jZXNzICYmIHByb2Nlc3MudmVyc2lvbnMgJiYgcHJvY2Vzcy52ZXJzaW9ucy5lbGVjdHJvbjtcblxuICAgIGlmIChmaXJlZm94KSB7XG4gICAgICAgIHZhciBtYXRjaCA9IG5hdmlnYXRvci51c2VyQWdlbnQubWF0Y2goL0ZpcmVmb3hcXC8oXFxkK1xcLlxcZCspLyk7XG4gICAgICAgIGlmIChtYXRjaCAmJiBtYXRjaFsxXSAmJiBOdW1iZXIobWF0Y2hbMV0pKSB7XG4gICAgICAgICAgICBmaXJlZm94VmVyc2lvbiA9IE51bWJlcihtYXRjaFsxXSk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIGNocm9tZSB8fCBmaXJlZm94VmVyc2lvbiA+PSAzMS4wIHx8IGVsZWN0cm9uO1xuICB9XG5cbiAgdmFyIHlpZWxkQ29sb3IgPSBmdW5jdGlvbigpIHtcbiAgICB2YXIgZ29sZGVuUmF0aW8gPSAwLjYxODAzMzk4ODc0OTg5NTtcbiAgICBodWUgKz0gZ29sZGVuUmF0aW87XG4gICAgaHVlID0gaHVlICUgMTtcbiAgICByZXR1cm4gaHVlICogMzYwO1xuICB9O1xuXG4gIHZhciBpbk5vZGUgPSB0eXBlb2Ygd2luZG93ID09PSAndW5kZWZpbmVkJyxcbiAgICAgIGxzID0gIWluTm9kZSAmJiB3aW5kb3cubG9jYWxTdG9yYWdlLFxuICAgICAgZGVidWdLZXkgPSBscy5hbmRsb2dLZXkgfHwgJ2RlYnVnJyxcbiAgICAgIGRlYnVnID0gbHNbZGVidWdLZXldLFxuICAgICAgbG9nZ2VyID0gcmVxdWlyZSgnYW5kbG9nJyksXG4gICAgICBiaW5kID0gRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQsXG4gICAgICBodWUgPSAwLFxuICAgICAgcGFkZGluZyA9IHRydWUsXG4gICAgICBzZXBhcmF0b3IgPSAnfCcsXG4gICAgICBwYWRMZW5ndGggPSAxNSxcbiAgICAgIG5vb3AgPSBmdW5jdGlvbigpIHt9LFxuICAgICAgLy8gaWYgbHMuZGVidWdDb2xvcnMgaXMgc2V0LCB1c2UgdGhhdCwgb3RoZXJ3aXNlIGNoZWNrIGZvciBzdXBwb3J0XG4gICAgICBjb2xvcnNTdXBwb3J0ZWQgPSBscy5kZWJ1Z0NvbG9ycyA/IChscy5kZWJ1Z0NvbG9ycyAhPT0gXCJmYWxzZVwiKSA6IGNoZWNrQ29sb3JTdXBwb3J0KCksXG4gICAgICBib3dzID0gbnVsbCxcbiAgICAgIGRlYnVnUmVnZXggPSBudWxsLFxuICAgICAgaW52ZXJ0UmVnZXggPSBmYWxzZSxcbiAgICAgIG1vZHVsZUNvbG9yc01hcCA9IHt9O1xuXG4gIGlmIChkZWJ1ZyAmJiBkZWJ1Z1swXSA9PT0gJyEnICYmIGRlYnVnWzFdID09PSAnLycpIHtcbiAgICBpbnZlcnRSZWdleCA9IHRydWU7XG4gICAgZGVidWcgPSBkZWJ1Zy5zbGljZSgxKTtcbiAgfVxuICBkZWJ1Z1JlZ2V4ID0gZGVidWcgJiYgZGVidWdbMF09PT0nLycgJiYgbmV3IFJlZ0V4cChkZWJ1Zy5zdWJzdHJpbmcoMSxkZWJ1Zy5sZW5ndGgtMSkpO1xuXG4gIHZhciBsb2dMZXZlbHMgPSBbJ2xvZycsICdkZWJ1ZycsICd3YXJuJywgJ2Vycm9yJywgJ2luZm8nXTtcblxuICAvL05vb3Agc2hvdWxkIG5vb3BcbiAgZm9yICh2YXIgaSA9IDAsIGlpID0gbG9nTGV2ZWxzLmxlbmd0aDsgaSA8IGlpOyBpKyspIHtcbiAgICAgIG5vb3BbIGxvZ0xldmVsc1tpXSBdID0gbm9vcDtcbiAgfVxuXG4gIGJvd3MgPSBmdW5jdGlvbihzdHIpIHtcbiAgICB2YXIgbXNnLCBjb2xvclN0cmluZywgbG9nZm47XG5cbiAgICBpZiAocGFkZGluZykge1xuICAgICAgbXNnID0gKHN0ci5zbGljZSgwLCBwYWRMZW5ndGgpKTtcbiAgICAgIG1zZyArPSBBcnJheShwYWRMZW5ndGggKyAzIC0gbXNnLmxlbmd0aCkuam9pbignICcpICsgc2VwYXJhdG9yO1xuICAgIH0gZWxzZSB7XG4gICAgICBtc2cgPSBzdHIgKyBBcnJheSgzKS5qb2luKCcgJykgKyBzZXBhcmF0b3I7XG4gICAgfVxuXG4gICAgaWYgKGRlYnVnUmVnZXgpIHtcbiAgICAgICAgdmFyIG1hdGNoZXMgPSBzdHIubWF0Y2goZGVidWdSZWdleCk7XG4gICAgICAgIGlmIChcbiAgICAgICAgICAgICghaW52ZXJ0UmVnZXggJiYgIW1hdGNoZXMpIHx8XG4gICAgICAgICAgICAoaW52ZXJ0UmVnZXggJiYgbWF0Y2hlcylcbiAgICAgICAgKSByZXR1cm4gbm9vcDtcbiAgICB9XG5cbiAgICBpZiAoIWJpbmQpIHJldHVybiBub29wO1xuXG4gICAgdmFyIGxvZ0FyZ3MgPSBbbG9nZ2VyXTtcbiAgICBpZiAoY29sb3JzU3VwcG9ydGVkKSB7XG4gICAgICBpZighbW9kdWxlQ29sb3JzTWFwW3N0cl0pe1xuICAgICAgICBtb2R1bGVDb2xvcnNNYXBbc3RyXT0geWllbGRDb2xvcigpO1xuICAgICAgfVxuICAgICAgdmFyIGNvbG9yID0gbW9kdWxlQ29sb3JzTWFwW3N0cl07XG4gICAgICBtc2cgPSBcIiVjXCIgKyBtc2c7XG4gICAgICBjb2xvclN0cmluZyA9IFwiY29sb3I6IGhzbChcIiArIChjb2xvcikgKyBcIiw5OSUsNDAlKTsgZm9udC13ZWlnaHQ6IGJvbGRcIjtcblxuICAgICAgbG9nQXJncy5wdXNoKG1zZywgY29sb3JTdHJpbmcpO1xuICAgIH1lbHNle1xuICAgICAgbG9nQXJncy5wdXNoKG1zZyk7XG4gICAgfVxuXG4gICAgaWYoYXJndW1lbnRzLmxlbmd0aD4xKXtcbiAgICAgICAgdmFyIGFyZ3MgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpO1xuICAgICAgICBsb2dBcmdzID0gbG9nQXJncy5jb25jYXQoYXJncyk7XG4gICAgfVxuXG4gICAgbG9nZm4gPSBiaW5kLmFwcGx5KGxvZ2dlci5sb2csIGxvZ0FyZ3MpO1xuXG4gICAgbG9nTGV2ZWxzLmZvckVhY2goZnVuY3Rpb24gKGYpIHtcbiAgICAgIGxvZ2ZuW2ZdID0gYmluZC5hcHBseShsb2dnZXJbZl0gfHwgbG9nZm4sIGxvZ0FyZ3MpO1xuICAgIH0pO1xuICAgIHJldHVybiBsb2dmbjtcbiAgfTtcblxuICBib3dzLmNvbmZpZyA9IGZ1bmN0aW9uKGNvbmZpZykge1xuICAgIGlmIChjb25maWcucGFkTGVuZ3RoKSB7XG4gICAgICBwYWRMZW5ndGggPSBjb25maWcucGFkTGVuZ3RoO1xuICAgIH1cblxuICAgIGlmICh0eXBlb2YgY29uZmlnLnBhZGRpbmcgPT09ICdib29sZWFuJykge1xuICAgICAgcGFkZGluZyA9IGNvbmZpZy5wYWRkaW5nO1xuICAgIH1cblxuICAgIGlmIChjb25maWcuc2VwYXJhdG9yKSB7XG4gICAgICBzZXBhcmF0b3IgPSBjb25maWcuc2VwYXJhdG9yO1xuICAgIH0gZWxzZSBpZiAoY29uZmlnLnNlcGFyYXRvciA9PT0gZmFsc2UgfHwgY29uZmlnLnNlcGFyYXRvciA9PT0gJycpIHtcbiAgICAgIHNlcGFyYXRvciA9ICcnXG4gICAgfVxuICB9O1xuXG4gIGlmICh0eXBlb2YgbW9kdWxlICE9PSAndW5kZWZpbmVkJykge1xuICAgIG1vZHVsZS5leHBvcnRzID0gYm93cztcbiAgfSBlbHNlIHtcbiAgICB3aW5kb3cuYm93cyA9IGJvd3M7XG4gIH1cbn0pLmNhbGwoKTtcblxufSkuY2FsbCh0aGlzLHJlcXVpcmUoXCJySDFKUEdcIikpIiwiLy8gc2hpbSBmb3IgdXNpbmcgcHJvY2VzcyBpbiBicm93c2VyXG5cbnZhciBwcm9jZXNzID0gbW9kdWxlLmV4cG9ydHMgPSB7fTtcblxucHJvY2Vzcy5uZXh0VGljayA9IChmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGNhblNldEltbWVkaWF0ZSA9IHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnXG4gICAgJiYgd2luZG93LnNldEltbWVkaWF0ZTtcbiAgICB2YXIgY2FuUG9zdCA9IHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnXG4gICAgJiYgd2luZG93LnBvc3RNZXNzYWdlICYmIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyXG4gICAgO1xuXG4gICAgaWYgKGNhblNldEltbWVkaWF0ZSkge1xuICAgICAgICByZXR1cm4gZnVuY3Rpb24gKGYpIHsgcmV0dXJuIHdpbmRvdy5zZXRJbW1lZGlhdGUoZikgfTtcbiAgICB9XG5cbiAgICBpZiAoY2FuUG9zdCkge1xuICAgICAgICB2YXIgcXVldWUgPSBbXTtcbiAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ21lc3NhZ2UnLCBmdW5jdGlvbiAoZXYpIHtcbiAgICAgICAgICAgIHZhciBzb3VyY2UgPSBldi5zb3VyY2U7XG4gICAgICAgICAgICBpZiAoKHNvdXJjZSA9PT0gd2luZG93IHx8IHNvdXJjZSA9PT0gbnVsbCkgJiYgZXYuZGF0YSA9PT0gJ3Byb2Nlc3MtdGljaycpIHtcbiAgICAgICAgICAgICAgICBldi5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgICAgICAgICBpZiAocXVldWUubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgICAgICB2YXIgZm4gPSBxdWV1ZS5zaGlmdCgpO1xuICAgICAgICAgICAgICAgICAgICBmbigpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSwgdHJ1ZSk7XG5cbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uIG5leHRUaWNrKGZuKSB7XG4gICAgICAgICAgICBxdWV1ZS5wdXNoKGZuKTtcbiAgICAgICAgICAgIHdpbmRvdy5wb3N0TWVzc2FnZSgncHJvY2Vzcy10aWNrJywgJyonKTtcbiAgICAgICAgfTtcbiAgICB9XG5cbiAgICByZXR1cm4gZnVuY3Rpb24gbmV4dFRpY2soZm4pIHtcbiAgICAgICAgc2V0VGltZW91dChmbiwgMCk7XG4gICAgfTtcbn0pKCk7XG5cbnByb2Nlc3MudGl0bGUgPSAnYnJvd3Nlcic7XG5wcm9jZXNzLmJyb3dzZXIgPSB0cnVlO1xucHJvY2Vzcy5lbnYgPSB7fTtcbnByb2Nlc3MuYXJndiA9IFtdO1xuXG5mdW5jdGlvbiBub29wKCkge31cblxucHJvY2Vzcy5vbiA9IG5vb3A7XG5wcm9jZXNzLmFkZExpc3RlbmVyID0gbm9vcDtcbnByb2Nlc3Mub25jZSA9IG5vb3A7XG5wcm9jZXNzLm9mZiA9IG5vb3A7XG5wcm9jZXNzLnJlbW92ZUxpc3RlbmVyID0gbm9vcDtcbnByb2Nlc3MucmVtb3ZlQWxsTGlzdGVuZXJzID0gbm9vcDtcbnByb2Nlc3MuZW1pdCA9IG5vb3A7XG5cbnByb2Nlc3MuYmluZGluZyA9IGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdwcm9jZXNzLmJpbmRpbmcgaXMgbm90IHN1cHBvcnRlZCcpO1xufVxuXG4vLyBUT0RPKHNodHlsbWFuKVxucHJvY2Vzcy5jd2QgPSBmdW5jdGlvbiAoKSB7IHJldHVybiAnLycgfTtcbnByb2Nlc3MuY2hkaXIgPSBmdW5jdGlvbiAoZGlyKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdwcm9jZXNzLmNoZGlyIGlzIG5vdCBzdXBwb3J0ZWQnKTtcbn07XG4iLCJcbnZhciBsb2cgPSByZXF1aXJlKCdib3dzJykoJ0FwcC5Db25maWcnKTtcbnZhciBBcHAgPSB3aW5kb3cuQXBwIHx8IHt9O1xuXG5BcHAuQ29uZmlnID0gKGZ1bmN0aW9uICgpIHtcblxuXHR2YXIgbG9jYWxEYXRhUGF0aCA9ICcuLi9kYXRhLyc7XG5cbiAgICB2YXIgaW5pdCA9IGZ1bmN0aW9uKCkge1xuICAgIFx0bG9nKCdpbml0Jyk7XG4gICAgfTtcblxuICAgIHJldHVybiB7XG4gICAgICAgIEluaXQgOiBpbml0LFxuXG4gICAgICAgIEdldExvY2FsRGF0YVBhdGggOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIFx0cmV0dXJuIGxvY2FsRGF0YVBhdGg7XG4gICAgICAgIH1cbiAgICB9O1xuXG59KCkpOyIsIlxudmFyIGxvZyA9IHJlcXVpcmUoJ2Jvd3MnKSgnQXBwLlV0aWxzJyk7XG52YXIgQXBwID0gd2luZG93LkFwcCB8fCB7fTtcblxuQXBwLlV0aWxzID0gKGZ1bmN0aW9uICgpIHtcblxuICAgIHZhciBpbml0ID0gZnVuY3Rpb24oKSB7XG4gICAgXHRsb2coJ2luaXQnKTtcbiAgICB9O1xuXG4gICAgdmFyIGdldElucHV0RGF0YSA9IGZ1bmN0aW9uICh1cmwsIGxvY2FsLCBjYikge1xuICAgICAgICB2YXIgcGF0aCA9IChsb2NhbCkgPyBBcHAuQ29uZmlnLkdldExvY2FsRGF0YVBhdGgoKSA6ICcnO1xuICAgICAgICB2YXIgcmVxdWVzdCA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xuXG5cbiAgICAgICAgcmVxdWVzdC5vbmxvYWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBpZiAocmVxdWVzdC5zdGF0dXMgPj0gMjAwICYmIHJlcXVlc3Quc3RhdHVzIDwgNDAwKSB7XG4gICAgICAgICAgICAgICAgdmFyIHJlc3BvbnNlID0gcmVxdWVzdC5yZXNwb25zZVRleHQ7XG5cbiAgICAgICAgICAgICAgICBjYihyZXNwb25zZSk7XG5cbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgbG9nKCdTZXJ2ZXIgZXJyb3InKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcblxuICAgICAgICByZXF1ZXN0Lm9uZXJyb3IgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBsb2coJ0Nvbm5lY3Rpb24gZXJyb3InKTtcbiAgICAgICAgfTtcblxuICAgICAgICByZXF1ZXN0Lm9wZW4oJ0dFVCcsIHBhdGggKyB1cmwsIHRydWUpO1xuXG4gICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcbiAgICAgICAgICAgIHJlcXVlc3Quc2VuZCgpO1xuICAgICAgICB9LCAwKTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIHtcbiAgICAgICAgSW5pdCA6IGluaXQsXG4gICAgICAgIEdldElucHV0RGF0YSA6IGdldElucHV0RGF0YVxuICAgIH07XG5cbn0oKSk7IiwiXG52YXIgbG9nID0gcmVxdWlyZSgnYm93cycpKCdBcHAuRGF5T25lJyk7XG52YXIgQXBwID0gd2luZG93LkFwcCB8fCB7fTtcblxuQXBwLkRheU9uZSA9IChmdW5jdGlvbiAoKSB7XG5cbiAgICB2YXIgaW5wdXREYXRhID0gW10sXG4gICAgICAgIGRpcmVjdGlvbiA9IFt7XG4gICAgICAgICAgICAgICAgZGlyOiAnbm9ydGgnLFxuICAgICAgICAgICAgICAgIGF4aXMgOiAneScsXG4gICAgICAgICAgICAgICAgdmFsIDogMSxcbiAgICAgICAgICAgICAgICBkZWcgOiAwXG4gICAgICAgICAgICB9LCB7XG4gICAgICAgICAgICAgICAgZGlyOiAnZWFzdCcsXG4gICAgICAgICAgICAgICAgYXhpcyA6ICd4JyxcbiAgICAgICAgICAgICAgICB2YWwgOiAxLFxuICAgICAgICAgICAgICAgIGRlZyA6IDkwXG4gICAgICAgICAgICB9LCB7XG4gICAgICAgICAgICAgICAgZGlyOiAnc291dGgnLFxuICAgICAgICAgICAgICAgIGF4aXMgOiAneScsXG4gICAgICAgICAgICAgICAgdmFsIDogLTEsXG4gICAgICAgICAgICAgICAgZGVnIDogMTgwXG4gICAgICAgICAgICB9LCB7XG4gICAgICAgICAgICAgICAgZGlyOiAnd2VzdCcsXG4gICAgICAgICAgICAgICAgYXhpcyA6ICd4JyxcbiAgICAgICAgICAgICAgICB2YWwgOiAtMSxcbiAgICAgICAgICAgICAgICBkZWcgOiAtOTBcbiAgICAgICAgICAgIH1cbiAgICAgICAgXSxcbiAgICAgICAgcG9zID0geyBcbiAgICAgICAgICAgIHggOiAwLCBcbiAgICAgICAgICAgIHkgOiAwIFxuICAgICAgICB9LFxuICAgICAgICBicmVhZGNydW1iID0gW10sXG4gICAgICAgIGN1cnJlbnREaXJJbmRleCA9IDAsIC8vIHN0YXJ0aW5nIGRpcmVjdGlvbiBpcyAnbm9ydGgnLCBpbmRleCAwIGluIGRpcmVjdGlvbiBhcnJheVxuICAgICAgICBjdXJyZW50RGVncmVlID0gMCwgLy8gc3RhcnRpbmcgZGlyZWN0aW9uIGlzICdub3J0aCcsIHJvdGF0aW9uIGlzIDBcbiAgICAgICAgYmxvY2tzID0gMCxcbiAgICAgICAgc3RlcFNpemUgPSAxMCxcbiAgICAgICAgY2l0eVNpemUgPSA0MDAwLFxuICAgICAgICBjaXR5R3JpZEVsLFxuICAgICAgICBzYW50YUVsO1xuXG4gICAgdmFyIGluaXQgPSBmdW5jdGlvbiAoKSB7XG4gICAgXHRsb2coJ2luaXQnKTtcblxuICAgICAgICBjaXR5R3JpZEVsID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI2NpdHktZ3JpZCcpO1xuICAgICAgICBjaXR5R3JpZEVsLnN0eWxlLndpZHRoID0gY2l0eVNpemUgKyAncHgnO1xuICAgICAgICBjaXR5R3JpZEVsLnN0eWxlLmhlaWdodCA9IGNpdHlTaXplICsgJ3B4JztcblxuICAgICAgICBzYW50YUVsID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI3NhbnRhJyk7XG5cbiAgICAgICAgY3JlYXRlR3JpZHMoKTtcbiAgICAgICAgQXBwLlV0aWxzLkdldElucHV0RGF0YSgnMS50eHQnLCB0cnVlLCBvbklucHV0RGF0YVJlYWR5KTtcbiAgICB9O1xuXG4gICAgdmFyIG9uSW5wdXREYXRhUmVhZHkgPSBmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICBpbnB1dERhdGEgPSBkYXRhLnNwbGl0KCcsICcpO1xuXG4gICAgICAgIGlucHV0RGF0YS5mb3JFYWNoKGZ1bmN0aW9uKGQsIGkpe1xuICAgICAgICAgICAgdmFyIHR1cm4gPSBkLnN1YnN0cigwLCAxKSxcbiAgICAgICAgICAgICAgICBzdGVwcyA9IHBhcnNlSW50KGQuc3Vic3RyKDEpKTtcblxuICAgICAgICAgICAgY3VycmVudERpckluZGV4ID0gZ2V0RGlyZWN0aW9uSW5kZXgodHVybik7XG5cbiAgICAgICAgICAgIHZhciBjYXJkaW5hbE9iaiA9IGRpcmVjdGlvbltjdXJyZW50RGlySW5kZXhdLFxuICAgICAgICAgICAgICAgIHMgPSBzdGVwcztcblxuICAgICAgICAgICAgd2hpbGUocyA+IDApIHtcbiAgICAgICAgICAgICAgICB2YXIgcHJldlBvcyA9IHt9O1xuXG4gICAgICAgICAgICAgICAgcHJldlBvcy54ID0gcG9zLng7XG4gICAgICAgICAgICAgICAgcHJldlBvcy55ID0gcG9zLnk7XG4gICAgICAgICAgICAgICAgYnJlYWRjcnVtYi5wdXNoKHByZXZQb3MpO1xuXG4gICAgICAgICAgICAgICAgcG9zW2NhcmRpbmFsT2JqLmF4aXNdICs9IGNhcmRpbmFsT2JqLnZhbDtcblxuICAgICAgICAgICAgICAgIHMtLTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgY3JlYXRlUm91dGUoc3RlcHMpO1xuXG4gICAgICAgICAgICAvL2xvZyhkLCBwb3MsIGNhcmRpbmFsT2JqLmRpcik7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGJsb2NrcyA9IE1hdGguYWJzKHBvcy54KSArIE1hdGguYWJzKHBvcy55KTtcblxuICAgICAgICAvLyBNb3ZlIFNhbnRhIHRvIGZpbmFsIHBvc2l0aW9uICYgZmFjaW5nIHRoZSBmaW5hbCBkaXJlY3Rpb25cbiAgICAgICAgc2FudGFFbC5jbGFzc0xpc3QuYWRkKGRpcmVjdGlvbltjdXJyZW50RGlySW5kZXhdLmRpcik7XG4gICAgICAgIHNhbnRhRWwuc3R5bGUudHJhbnNmb3JtID0gJ3JvdGF0ZSgnICsgZGlyZWN0aW9uW2N1cnJlbnREaXJJbmRleF0uZGVnICsgJ2RlZyknO1xuICAgICAgICBzYW50YUVsLnN0eWxlLnRvcCA9IChwb3MueSAqIHN0ZXBTaXplICogLTEpICsgJ3B4JztcbiAgICAgICAgc2FudGFFbC5zdHlsZS5sZWZ0ID0gKHBvcy54ICogc3RlcFNpemUpICsgJ3B4JztcbiAgICAgICAgXG4gICAgICAgIG91dHB1dEFuc3dlcignW0RheSAxXVtQYXJ0IDFdIDogJyArIGJsb2NrcyArICcgYmxvY2tzIGF3YXkgZmFjaW5nICcgKyBkaXJlY3Rpb25bY3VycmVudERpckluZGV4XS5kaXIpO1xuICAgICAgICBmaW5kSHFQb3MoKTtcbiAgICB9O1xuXG4gICAgdmFyIGdldERpcmVjdGlvbkluZGV4ID0gZnVuY3Rpb24odHVybikge1xuICAgICAgICB2YXIgZGlyID0gY3VycmVudERpckluZGV4O1xuXG4gICAgICAgIHR1cm4gPSB0dXJuLnRvVXBwZXJDYXNlKCk7XG5cbiAgICAgICAgaWYodHVybiA9PT0gJ1InKSB7XG4gICAgICAgICAgICBkaXIgKz0gMTtcbiAgICAgICAgICAgIGRpciA9IChkaXIgPiBkaXJlY3Rpb24ubGVuZ3RoLTEpID8gMCA6IGRpcjtcbiAgICAgICAgfSBcbiAgICAgICAgZWxzZSBpZih0dXJuID09PSAnTCcpe1xuICAgICAgICAgICAgZGlyIC09IDE7XG4gICAgICAgICAgICBkaXIgPSAoZGlyIDwgMCkgPyBkaXJlY3Rpb24ubGVuZ3RoLTEgOiBkaXI7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gZGlyO1xuICAgIH07XG5cbiAgICB2YXIgZmluZFJlcGVhdFBvcyA9IGZ1bmN0aW9uKHRhcmdldFBvcykge1xuICAgICAgICByZXR1cm4gYnJlYWRjcnVtYi5maWx0ZXIoZnVuY3Rpb24gKG8pIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdGFyZ2V0UG9zLnggPT09IG8ueCAmJiB0YXJnZXRQb3MueSA9PT0gby55O1xuICAgICAgICAgICAgfSk7XG4gICAgfTtcblxuICAgIHZhciBmaW5kSHFQb3MgPSBmdW5jdGlvbigpIHtcblxuICAgICAgICB2YXIgcmVwZWF0UG9zO1xuXG4gICAgICAgIC8vIExvb3AgdGhyb3VnaCBlYWNoIGJyZWFkY3J1bSB0byBmaW5kIDFzdCByZXBlYXRlZCBwb3NpdGlvblxuICAgICAgICB3aGlsZShicmVhZGNydW1iLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgIHZhciB0YXJnZXRQb3MgPSBicmVhZGNydW1iLnNoaWZ0KCk7XG5cbiAgICAgICAgICAgIHJlcGVhdFBvcyA9IGZpbmRSZXBlYXRQb3ModGFyZ2V0UG9zKTtcblxuICAgICAgICAgICAgaWYgKHJlcGVhdFBvcy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgdmFyIGhxQmxvY2sgPSBNYXRoLmFicyhyZXBlYXRQb3NbMF0ueCkgKyBNYXRoLmFicyhyZXBlYXRQb3NbMF0ueSksXG4gICAgICAgICAgICAgICAgICAgIGhxRWwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjaHEnKTtcblxuICAgICAgICAgICAgICAgIG91dHB1dEFuc3dlcignW0RheSAxXVtQYXJ0IDJdIDogRWFzdGVyIEJ1bm55IEhRIGlzICcgKyBocUJsb2NrICsgJyBibG9ja3MgYXdheScpO1xuICAgICAgICAgICAgICAgIGhxRWwuc3R5bGUubGVmdCA9IChyZXBlYXRQb3NbMF0ueCAqIHN0ZXBTaXplKSArICdweCc7XG4gICAgICAgICAgICAgICAgaHFFbC5zdHlsZS50b3AgPSAocmVwZWF0UG9zWzBdLnkgKiBzdGVwU2l6ZSAqIC0xKSArICdweCc7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9O1xuXG4gICAgdmFyIG91dHB1dEFuc3dlciA9IGZ1bmN0aW9uKGFuc3dlcikge1xuICAgICAgICB2YXIgYW5zd2VyTGlzdCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5hbnN3ZXJzJyksXG4gICAgICAgICAgICBlbCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2xpJyk7XG5cbiAgICAgICAgZWwuaW5uZXJIVE1MID0gYW5zd2VyO1xuICAgICAgICBhbnN3ZXJMaXN0LmFwcGVuZENoaWxkKGVsKTtcbiAgICB9O1xuXG4gICAgdmFyIGNyZWF0ZUdyaWRzID0gZnVuY3Rpb24oKSB7XG4gICAgICAgIHZhciBudW1Db2wgPSBjaXR5U2l6ZSAvIHN0ZXBTaXplLFxuICAgICAgICAgICAgcm93c0VsID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI3Jvd3MnKSxcbiAgICAgICAgICAgIGNvbHNFbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNjb2xzJyk7XG5cbiAgICAgICAgZm9yKHZhciBpPTA7IGkgPCBudW1Db2w7IGkrKykge1xuICAgICAgICAgICAgdmFyIHJvd0Jsb2NrRWwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICAgICAgICAgIHJvd0Jsb2NrRWwuY2xhc3NMaXN0LmFkZCgnYmxvY2snLCAncm93Jyk7XG4gICAgICAgICAgICByb3dCbG9ja0VsLnN0eWxlLnRvcCA9IChzdGVwU2l6ZSAqIGkpICsgJ3B4JztcbiAgICAgICAgICAgIHJvd3NFbC5hcHBlbmRDaGlsZChyb3dCbG9ja0VsKTtcblxuICAgICAgICAgICAgdmFyIGNvbEJsb2NrRWwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICAgICAgICAgIGNvbEJsb2NrRWwuY2xhc3NMaXN0LmFkZCgnYmxvY2snLCAnY29sJyk7XG4gICAgICAgICAgICBjb2xCbG9ja0VsLnN0eWxlLmxlZnQgPSAoc3RlcFNpemUgKiBpKSArICdweCc7XG4gICAgICAgICAgICBjb2xzRWwuYXBwZW5kQ2hpbGQoY29sQmxvY2tFbCk7XG4gICAgICAgIH1cbiAgICB9O1xuXG4gICAgdmFyIGNyZWF0ZVJvdXRlID0gZnVuY3Rpb24oc3RlcHMpIHtcbiAgICAgICAgdmFyIHN0ZXBzSW5QeCA9IHN0ZXBzICogc3RlcFNpemUsXG4gICAgICAgICAgICBwcmV2UG9zID0gYnJlYWRjcnVtYlticmVhZGNydW1iLmxlbmd0aCAtIHN0ZXBzXSxcbiAgICAgICAgICAgIHJvdXRlQ29udGFpbmVyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI3JvdXRlJyksXG4gICAgICAgICAgICBsaW5lRWwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcblxuICAgICAgICBsaW5lRWwuY2xhc3NMaXN0LmFkZCgnbGluZScpO1xuXG4gICAgICAgIHZhciBzdGFydFBvcywgbGluZUhlaWdodCwgbGluZVdpZHRoO1xuXG4gICAgICAgIGlmKHByZXZQb3MueCAtIHBvcy54ICE9PSAwKSB7XG4gICAgICAgICAgICBsaW5lV2lkdGggPSBzdGVwc0luUHg7XG4gICAgICAgICAgICBsaW5lSGVpZ2h0ID0gMTtcblxuICAgICAgICAgICAgaWYocHJldlBvcy54IDwgcG9zLngpIHtcbiAgICAgICAgICAgICAgICBzdGFydFBvcyA9IHByZXZQb3M7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHN0YXJ0UG9zID0gcG9zO1xuICAgICAgICAgICAgfVxuICAgICAgICB9IGVsc2UgaWYocHJldlBvcy55IC0gcG9zLnkgIT09IDApIHtcbiAgICAgICAgICAgIGxpbmVXaWR0aCA9IDE7XG4gICAgICAgICAgICBsaW5lSGVpZ2h0ID0gc3RlcHNJblB4O1xuXG4gICAgICAgICAgICBpZihwcmV2UG9zLnkgPiBwb3MueSkge1xuICAgICAgICAgICAgICAgIHN0YXJ0UG9zID0gcHJldlBvcztcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgc3RhcnRQb3MgPSBwb3M7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIGxpbmVFbC5zdHlsZS50b3AgPSAoc3RhcnRQb3MueSAqIHN0ZXBTaXplICogLTEpICsgJ3B4JztcbiAgICAgICAgbGluZUVsLnN0eWxlLmxlZnQgPSAoc3RhcnRQb3MueCAqIHN0ZXBTaXplKSArICdweCc7XG4gICAgICAgIGxpbmVFbC5zdHlsZS53aWR0aCA9IGxpbmVXaWR0aCArICdweCc7XG4gICAgICAgIGxpbmVFbC5zdHlsZS5oZWlnaHQgPSBsaW5lSGVpZ2h0ICsgJ3B4JztcblxuICAgICAgICByb3V0ZUNvbnRhaW5lci5hcHBlbmRDaGlsZChsaW5lRWwpO1xuICAgIH07XG5cbiAgICByZXR1cm4ge1xuICAgICAgICBJbml0IDogaW5pdFxuICAgIH07XG5cbn0oKSk7IiwiXG52YXIgbG9nID0gcmVxdWlyZSgnYm93cycpKCdBcHAuRGF5VGhyZWUnKTtcbnZhciBBcHAgPSB3aW5kb3cuQXBwIHx8IHt9O1xuXG5BcHAuRGF5VGhyZWUgPSAoZnVuY3Rpb24gKCkge1xuXG4gICAgdmFyIGlucHV0RGF0YSA9IFtdO1xuXG4gICAgdmFyIGluaXQgPSBmdW5jdGlvbiAoKSB7XG4gICAgXHRsb2coJ2luaXQnKTtcblxuXG4gICAgICAgIEFwcC5VdGlscy5HZXRJbnB1dERhdGEoJzMudHh0JywgdHJ1ZSwgb25JbnB1dERhdGFSZWFkeSk7XG4gICAgfTtcblxuICAgIHZhciBvbklucHV0RGF0YVJlYWR5ID0gZnVuY3Rpb24oZGF0YSkge1xuICAgIFx0dmFyIGRhdGFBcnIgPSBkYXRhLnNwbGl0KCdcXG4nKTtcblxuICAgIFx0ZGF0YUFyci5mb3JFYWNoKGZ1bmN0aW9uKGNvZGVTdHIsIGkpe1xuICAgIFx0XHR2YXIgY29kZUFyciA9IGNvZGVTdHIuc3BsaXQoJycpO1xuXG4gICAgXHRcdGlucHV0RGF0YS5wdXNoKGNvZGVBcnIpO1xuICAgIFx0fSk7XG5cbiAgICBcdGxvZyhpbnB1dERhdGEpO1xuICAgIH07XG5cbiAgICB2YXIgb3V0cHV0QW5zd2VyID0gZnVuY3Rpb24oYW5zd2VyKSB7XG4gICAgICAgIHZhciBhbnN3ZXJMaXN0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmFuc3dlcnMnKSxcbiAgICAgICAgICAgIGVsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnbGknKTtcblxuICAgICAgICBlbC5pbm5lckhUTUwgPSBhbnN3ZXI7XG4gICAgICAgIGFuc3dlckxpc3QuYXBwZW5kQ2hpbGQoZWwpO1xuICAgIH07XG5cbiAgICByZXR1cm4ge1xuICAgICAgICBJbml0IDogaW5pdFxuICAgIH07XG5cbn0oKSk7XG4iLCJcbnZhciBsb2cgPSByZXF1aXJlKCdib3dzJykoJ0FwcC5EYXlUd28nKTtcbnZhciBBcHAgPSB3aW5kb3cuQXBwIHx8IHt9O1xuXG5BcHAuRGF5VHdvID0gKGZ1bmN0aW9uICgpIHtcblxuICAgIHZhciBpbnB1dERhdGEgPSBbXSxcbiAgICBcdGtleXBhZE1hdHJpeCA9IFtcblx0Ly9cdGNvbDogMCwgMSwgMlxuICAgIFx0XHRbMSwgMiwgM10sIC8vcm93IDBcbiAgICBcdFx0WzQsIDUsIDZdLCAvL3JvdyAxXG4gICAgXHRcdFs3LCA4LCA5XSAgLy9yb3cgMlxuICAgIFx0XSxcbiAgICBcdGFsdEtleXBhZE1hdHJpeCA9IFtcblx0Ly9cdGNvbDogIDAsICAxLCAgMiwgIDMsIDRcbiAgICBcdFx0Wy0xLCAtMSwgMSwgLTEsIC0xXSwgXHQgLy9yb3cgMFxuICAgIFx0XHRbLTEsIDIsIDMsIDQsIC0xXSwgXHRcdCAvL3JvdyAxXG4gICAgXHRcdFs1LCA2LCA3LCA4LCA5XSwgXHRcdCAvL3JvdyAyXG4gICAgXHRcdFstMSwgJ0EnLCAnQicsICdDJywgLTFdLCAvL3JvdyAzXG4gICAgXHRcdFstMSwgLTEsICdEJywgLTEsIC0xXSwgICAvL3JvdyA0XG4gICAgXHRdLFxuICAgIFx0Y3VycmVudFBvcyA9IHt9LFxuICAgIFx0b3V0cHV0UGFzcyA9IFtdLFxuICAgIFx0c3RhcnRLZXlOdW0gPSA1O1xuXG4gICAgdmFyIGluaXQgPSBmdW5jdGlvbiAoKSB7XG4gICAgXHRsb2coJ2luaXQnKTtcblxuXG4gICAgICAgIEFwcC5VdGlscy5HZXRJbnB1dERhdGEoJzIudHh0JywgdHJ1ZSwgb25JbnB1dERhdGFSZWFkeSk7XG4gICAgfTtcblxuICAgIHZhciBvbklucHV0RGF0YVJlYWR5ID0gZnVuY3Rpb24oZGF0YSkge1xuICAgIFx0dmFyIGRhdGFBcnIgPSBkYXRhLnNwbGl0KCdcXG4nKTtcblxuICAgIFx0Ly8gR2V0IHN0YXJ0aW5nIHBvc2l0aW9uIGJlZm9yZSBkZWNvZGVcbiAgICBcdGdldEluaXRQb3Moa2V5cGFkTWF0cml4KTtcblxuICAgIFx0ZGF0YUFyci5mb3JFYWNoKGZ1bmN0aW9uKGNvZGVTdHIsIGkpe1xuICAgIFx0XHR2YXIgY29kZUFyciA9IGNvZGVTdHIuc3BsaXQoJycpLFxuICAgIFx0XHRcdG91dHB1dERpZ2l0ID0gLTE7XG5cbiAgICBcdFx0aW5wdXREYXRhLnB1c2goY29kZUFycik7IC8vIHN0b3JlIGl0IGp1c3QgaW4gY2FzZVxuXG4gICAgXHRcdGNvZGVBcnIuZm9yRWFjaChmdW5jdGlvbihjb2RlLCBqKXtcbiAgICBcdFx0XHRkZWNvZGUoY29kZSwga2V5cGFkTWF0cml4KTtcbiAgICBcdFx0fSk7XG5cbiAgICBcdFx0b3V0cHV0RGlnaXQgPSBrZXlwYWRNYXRyaXhbY3VycmVudFBvcy5yb3ddW2N1cnJlbnRQb3MuY29sXTtcbiAgICBcdFx0b3V0cHV0UGFzcy5wdXNoKG91dHB1dERpZ2l0KTtcbiAgICBcdH0pO1xuXG4gICAgICAgIG91dHB1dEFuc3dlcignW0RheSAyXVtQYXJ0IDFdIDogQmF0aHJvb20gcGFzc2NvZGUgaXMgJyArIG91dHB1dFBhc3Muam9pbignJykpO1xuXG4gICAgXHQvLyBMZXRzIGZpbmQgb3V0IGFuc3dlciBmb3IgUGFydCAyXG4gICAgXHRhbHRLZXlwYWREZWNvZGUoKTtcbiAgICB9O1xuXG4gICAgdmFyIGdldEluaXRQb3MgPSBmdW5jdGlvbihrZXlwYWQpIHtcbiAgICBcdHZhciBpbml0UG9zID0ge307XG5cbiAgICBcdC8vIFNlYXJjaCAjNSdzIHBvc2l0aW9uIGluIHRoZSBzcGVjaWZpZWQga2V5cGFkIG1hdHJpeFxuICAgIFx0a2V5cGFkLmZvckVhY2goZnVuY3Rpb24oa2V5cGFkUm93LCByKXtcbiAgICBcdFx0dmFyIGMgPSBrZXlwYWRSb3cuaW5kZXhPZihzdGFydEtleU51bSk7XG5cblx0ICAgIFx0aWYoYyAhPT0gLTEpIHtcblx0ICAgIFx0XHRpbml0UG9zLmNvbCA9IGM7XG5cdCAgICBcdFx0aW5pdFBvcy5yb3cgPSByO1xuXHQgICAgXHR9XG4gICAgXHR9KTtcblxuICAgIFx0Y3VycmVudFBvcy5jb2wgPSBpbml0UG9zLmNvbDtcbiAgICBcdGN1cnJlbnRQb3Mucm93ID0gaW5pdFBvcy5yb3c7XG4gICAgfTtcblxuICAgIHZhciBhbHRLZXlwYWREZWNvZGUgPSBmdW5jdGlvbigpIHtcbiAgICBcdHZhciBvdXRwdXREaWdpdCA9IC0xO1xuXG4gICAgXHQvLyBFbXB0eSBvdXRwdXRQYXNzIGFycmF5XG4gICAgXHRvdXRwdXRQYXNzLmxlbmd0aCA9IDA7XG5cbiAgICBcdC8vIEdldCBzdGFydGluZyBwb3NpdGlvbiBiZWZvcmUgZGVjb2RlXG4gICAgXHRnZXRJbml0UG9zKGFsdEtleXBhZE1hdHJpeCk7XG5cbiAgICBcdGlucHV0RGF0YS5mb3JFYWNoKGZ1bmN0aW9uKGNvZGVBcnIsIGope1xuXHRcdFx0Y29kZUFyci5mb3JFYWNoKGZ1bmN0aW9uKGNvZGUsIGope1xuICAgIFx0XHRcdGRlY29kZShjb2RlLCBhbHRLZXlwYWRNYXRyaXgpO1xuICAgIFx0XHR9KTtcblxuXHRcdFx0b3V0cHV0RGlnaXQgPSBhbHRLZXlwYWRNYXRyaXhbY3VycmVudFBvcy5yb3ddW2N1cnJlbnRQb3MuY29sXTtcblx0XHRcdG91dHB1dFBhc3MucHVzaChvdXRwdXREaWdpdCk7XG5cdFx0fSk7XG5cbiAgICAgICAgb3V0cHV0QW5zd2VyKCdbRGF5IDJdW1BhcnQgMl0gOiBSZWFsIGJhdGhyb29tIHBhc3Njb2RlIGlzICcgKyBvdXRwdXRQYXNzLmpvaW4oJycpKTtcbiAgICB9O1xuXG4gICAgdmFyIGRlY29kZSA9IGZ1bmN0aW9uKGNvZGUsIGtleXBhZCkge1xuICAgIFx0dmFyIG5ld1BvcyA9IHtyb3cgOiBjdXJyZW50UG9zLnJvdywgY29sIDogY3VycmVudFBvcy5jb2x9O1xuXG4gICAgXHRzd2l0Y2goY29kZSkge1xuICAgIFx0XHRjYXNlICdVJzpcbiAgICBcdFx0XHRuZXdQb3Mucm93LS07XG4gICAgXHRcdFx0bmV3UG9zLnJvdyA9IChuZXdQb3Mucm93IDwgMCkgPyAwIDogbmV3UG9zLnJvdztcbiAgICBcdFx0YnJlYWs7XG4gICAgXHRcdGNhc2UgJ0QnOlxuICAgIFx0XHRcdG5ld1Bvcy5yb3crKztcbiAgICBcdFx0XHRuZXdQb3Mucm93ID0gKG5ld1Bvcy5yb3cgPiBrZXlwYWQubGVuZ3RoLTEpID8ga2V5cGFkLmxlbmd0aC0xIDogbmV3UG9zLnJvdztcbiAgICBcdFx0YnJlYWs7XG4gICAgXHRcdGNhc2UgJ0wnOlxuICAgIFx0XHRcdG5ld1Bvcy5jb2wtLTtcbiAgICBcdFx0XHRuZXdQb3MuY29sID0gKG5ld1Bvcy5jb2wgPCAwKSA/IDAgOiBuZXdQb3MuY29sO1xuICAgIFx0XHRicmVhaztcbiAgICBcdFx0Y2FzZSAnUic6XG4gICAgXHRcdFx0bmV3UG9zLmNvbCsrO1xuICAgIFx0XHRcdG5ld1Bvcy5jb2wgPSAobmV3UG9zLmNvbCA+IGtleXBhZFtuZXdQb3Mucm93XS5sZW5ndGgtMSkgPyBrZXlwYWRbbmV3UG9zLnJvd10ubGVuZ3RoLTEgOiBuZXdQb3MuY29sO1xuICAgIFx0XHRicmVhaztcbiAgICBcdH1cblxuICAgIFx0Ly8gaWYgbmV3IHBvc2l0aW9uIGxhbmRzIG9uIC0xIG9mIHRoZSBrZXlwYWQsIGl0IGlzIGFuIGludmFsaWQgcG9zaXRpb24sIGhlbmNlIGRvbid0IG1vdmVcblx0XHRpZihrZXlwYWRbbmV3UG9zLnJvd11bbmV3UG9zLmNvbF0gIT09IC0xKSB7XG5cdFx0XHRjdXJyZW50UG9zLnJvdyA9IG5ld1Bvcy5yb3c7XG5cdCAgICBcdGN1cnJlbnRQb3MuY29sID0gbmV3UG9zLmNvbDtcblx0ICAgIH1cbiAgICB9O1xuXG4gICAgdmFyIG91dHB1dEFuc3dlciA9IGZ1bmN0aW9uKGFuc3dlcikge1xuICAgICAgICB2YXIgYW5zd2VyTGlzdCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5hbnN3ZXJzJyksXG4gICAgICAgICAgICBlbCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2xpJyk7XG5cbiAgICAgICAgZWwuaW5uZXJIVE1MID0gYW5zd2VyO1xuICAgICAgICBhbnN3ZXJMaXN0LmFwcGVuZENoaWxkKGVsKTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIHtcbiAgICAgICAgSW5pdCA6IGluaXRcbiAgICB9O1xuXG59KCkpOyIsIlxuIC8vIE5PVEU6IFJ1biBcImxvY2FsU3RvcmFnZS5kZWJ1Zz10cnVlXCIgdG8gaGF2ZSBib3dzIHNob3dpbmcgdXAgaW4gY29uc29sZS5cbnZhciBsb2cgPSByZXF1aXJlKCdib3dzJykoJ0FwcCcpO1xuXG52YXIgQXBwID0gQXBwIHx8IHt9O1xud2luZG93LkFwcCA9IEFwcDtcblxudmFyIENvbmZpZyA9IHJlcXVpcmUoJy4vQ29uZmlnJyk7XG52YXIgVXRpbHMgPSByZXF1aXJlKCcuL1V0aWxzJyk7XG52YXIgRGF5T25lID0gcmVxdWlyZSgnLi9hZHZlbnRvZmNvZGUtMjAxNi9EYXlPbmUnKTtcbnZhciBEYXlUd28gPSByZXF1aXJlKCcuL2FkdmVudG9mY29kZS0yMDE2L0RheVR3bycpO1xudmFyIERheVRocmVlID0gcmVxdWlyZSgnLi9hZHZlbnRvZmNvZGUtMjAxNi9EYXlUaHJlZScpO1xuXG4vLyBBcHAgSW5pdGlhbGl6YXRpb25cbi8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG4oZnVuY3Rpb24gKCkge1xuXG5cdEFwcC5EYXlPbmUuSW5pdCgpO1xuXHRBcHAuRGF5VHdvLkluaXQoKTtcblx0QXBwLkRheVRocmVlLkluaXQoKTtcblxufSgpKTsiXX0=

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJhcHAuanMiXSwic291cmNlc0NvbnRlbnQiOlsiXG4gLy8gTk9URTogUnVuIFwibG9jYWxTdG9yYWdlLmRlYnVnPXRydWVcIiB0byBoYXZlIGJvd3Mgc2hvd2luZyB1cCBpbiBjb25zb2xlLlxudmFyIGxvZyA9IHJlcXVpcmUoJ2Jvd3MnKSgnQXBwJyk7XG5cbnZhciBBcHAgPSBBcHAgfHwge307XG53aW5kb3cuQXBwID0gQXBwO1xuXG52YXIgQ29uZmlnID0gcmVxdWlyZSgnLi9Db25maWcnKTtcbnZhciBVdGlscyA9IHJlcXVpcmUoJy4vVXRpbHMnKTtcbnZhciBEYXlPbmUgPSByZXF1aXJlKCcuL2FkdmVudG9mY29kZS0yMDE2L0RheU9uZScpO1xudmFyIERheVR3byA9IHJlcXVpcmUoJy4vYWR2ZW50b2Zjb2RlLTIwMTYvRGF5VHdvJyk7XG52YXIgRGF5VGhyZWUgPSByZXF1aXJlKCcuL2FkdmVudG9mY29kZS0yMDE2L0RheVRocmVlJyk7XG5cbi8vIEFwcCBJbml0aWFsaXphdGlvblxuLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbihmdW5jdGlvbiAoKSB7XG5cblx0QXBwLkRheU9uZS5Jbml0KCk7XG5cdEFwcC5EYXlUd28uSW5pdCgpO1xuXHRBcHAuRGF5VGhyZWUuSW5pdCgpO1xuXG59KCkpOyJdLCJmaWxlIjoiYXBwLmpzIn0=
