//
// Grunt Config
//

/*
 *  Environment Variables
 *  --------------------------------------------------
 */
var env = {
    isProduction: false,
};

var srcPath = './src/';
var distPath = './dist/';



/*
 *  Load Plugins
 *  --------------------------------------------------
 */
var gulp = require('gulp');

var plugins = require('gulp-load-plugins')({ // Load all dev dependencies in package.json except 'grunt related'.
    pattern: ['*', '!{grunt,gulp-grunt,jshint}'],
    scope: ['devDependencies']
});

/*
 *  Task: Truncate and delete the entire destination folder
 *  --------------------------------------------------
 */

gulp.task('clean', function () {
    return plugins.del([distPath + '**/*']);
});


/*
 *  Task: Copy non-compiling files to build folder
 *  --------------------------------------------------
 */
gulp.task('copy', function (next) {
    gulp.src([srcPath + '**/*.html'])
        .pipe(gulp.dest(distPath));

    gulp.src([srcPath + 'data/**/*'])
        .pipe(gulp.dest(distPath + 'data'));

    next();
});



/*
 *  Task: Sass Compilation
 *  --------------------------------------------------
 */
gulp.task('styles', function() {
    return gulp.src(srcPath + 'styles/**/*.scss')
        .pipe(plugins.if(!env.isProduction, plugins.sourcemaps.init()))
        .pipe(plugins.sass({
            errLogToConsole: true
        }))
        .pipe(plugins.autoprefixer({
            browsers: ['Firefox >= 32', 'Chrome >= 38', 'Explorer >= 10', 'iOS >= 7'],
            cascade: false
        }))
        .pipe(plugins.if(env.isProduction, plugins.minifyCss()))
        .pipe(plugins.if(!env.isProduction, plugins.sourcemaps.write()))
        .pipe(gulp.dest(distPath + 'css/'));
});



/*
 *  Task: Combine and compile server-side scripting for client-side purpose using Browserify
 *  --------------------------------------------------
 */
gulp.task('scripts', function(next) {
    //next(); //NOTE: nothing to compile at this stage

    gulp.src([srcPath + 'scripts/**/*.js'])
        .pipe(plugins.jshint({
            node: true, // Inform JSHint that this is a Node project to better fine tune error report
            browser: true,
        }))
        .pipe(plugins.jshint.reporter('default')); // May opt to use 'jshint-stylish' reporter.

    return gulp.src(srcPath + 'scripts/app.js')
        .pipe(plugins.if(!env.isProduction, plugins.sourcemaps.init()))
        .pipe(plugins.browserify({
            debug: !env.isProduction,
        }))
        .pipe(plugins.if(env.isProduction, plugins.uglify()))
        .pipe(plugins.if(!env.isProduction, plugins.sourcemaps.write()))
        .pipe(gulp.dest(distPath + 'js/'));
});



/*
 *  Task: Connect Server
 *  --------------------------------------------------
 */

gulp.task('server', function() {
    return plugins.browserSync({
        server: {
            baseDir: distPath
        },
        files: [
            distPath + '**/*.html',
            distPath + 'js/**/*.js'
        ],
        notify: false,
        ghostMode: false
    });
});


/*
 *  Task: Build
 *  --------------------------------------------------
 */
gulp.task('build-tasks', ['copy', 'scripts', 'styles'], function(next) {
    this.emit('build-tasks:done');
    next();
});

gulp.task('build', ['clean'], function (next) {
    gulp.start('build-tasks');
    this.on('build-tasks:done', next);
});



/*
 *  Task: Watch file changes on source folder and execute associated task(s).
 *  --------------------------------------------------
 */
gulp.task('watch', function() {
    gulp.watch([srcPath + '**/*.html', srcPath + 'data/**/*'], ['copy']);
    gulp.watch([srcPath + 'styles/**/*.scss'], ['styles']);
    gulp.watch([srcPath + 'scripts/**/*.js'], ['scripts']);
});



/*
 *  Task: Preparation for Production environment output
 *  --------------------------------------------------
 */
gulp.task('build-production', function() {
    env.isProduction = true;
    return gulp.start('build');
});



/*
 *  Default: By execute 'gulp', it will first 'build', then active 'server' and 'watch' in that sequence.
 *  --------------------------------------------------
 */
gulp.task('default', ['build'], function() {
    gulp.start('server');
    gulp.start('watch');
});