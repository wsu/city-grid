
var log = require('bows')('App.Config');
var App = window.App || {};

App.Config = (function () {

	var localDataPath = '../data/';

    var init = function() {
    	log('init');
    };

    return {
        Init : init,

        GetLocalDataPath : function () {
        	return localDataPath;
        }
    };

}());