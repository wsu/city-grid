
var log = require('bows')('App.Utils');
var App = window.App || {};

App.Utils = (function () {

    var init = function() {
    	log('init');
    };

    var getInputData = function (url, local, cb) {
        var path = (local) ? App.Config.GetLocalDataPath() : '';
        var request = new XMLHttpRequest();


        request.onload = function () {
            if (request.status >= 200 && request.status < 400) {
                var response = request.responseText;

                cb(response);

            } else {
                log('Server error');
            }
        };

        request.onerror = function () {
            log('Connection error');
        };

        request.open('GET', path + url, true);

        setTimeout(function(){
            request.send();
        }, 0);
    };

    return {
        Init : init,
        GetInputData : getInputData
    };

}());