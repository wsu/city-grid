
var log = require('bows')('App.DayOne');
var App = window.App || {};

App.DayOne = (function () {

    var inputData = [],
        direction = [{
                dir: 'north',
                axis : 'y',
                val : 1,
                deg : 0
            }, {
                dir: 'east',
                axis : 'x',
                val : 1,
                deg : 90
            }, {
                dir: 'south',
                axis : 'y',
                val : -1,
                deg : 180
            }, {
                dir: 'west',
                axis : 'x',
                val : -1,
                deg : -90
            }
        ],
        pos = { 
            x : 0, 
            y : 0 
        },
        breadcrumb = [],
        currentDirIndex = 0, // starting direction is 'north', index 0 in direction array
        currentDegree = 0, // starting direction is 'north', rotation is 0
        blocks = 0,
        stepSize = 10,
        citySize = 4000,
        cityGridEl,
        santaEl;

    var init = function () {
    	log('init');

        cityGridEl = document.querySelector('#city-grid');
        cityGridEl.style.width = citySize + 'px';
        cityGridEl.style.height = citySize + 'px';

        santaEl = document.querySelector('#santa');

        createGrids();
        App.Utils.GetInputData('1.txt', true, onInputDataReady);
    };

    var onInputDataReady = function (data) {
        inputData = data.split(', ');

        inputData.forEach(function(d, i){
            var turn = d.substr(0, 1),
                steps = parseInt(d.substr(1));

            currentDirIndex = getDirectionIndex(turn);

            var cardinalObj = direction[currentDirIndex],
                s = steps;

            while(s > 0) {
                var prevPos = {};

                prevPos.x = pos.x;
                prevPos.y = pos.y;
                breadcrumb.push(prevPos);

                pos[cardinalObj.axis] += cardinalObj.val;

                s--;
            }

            createRoute(steps);

            //log(d, pos, cardinalObj.dir);
        });

        blocks = Math.abs(pos.x) + Math.abs(pos.y);

        // Move Santa to final position & facing the final direction
        santaEl.classList.add(direction[currentDirIndex].dir);
        santaEl.style.transform = 'rotate(' + direction[currentDirIndex].deg + 'deg)';
        santaEl.style.top = (pos.y * stepSize * -1) + 'px';
        santaEl.style.left = (pos.x * stepSize) + 'px';
        
        outputAnswer('[Day 1][Part 1] : ' + blocks + ' blocks away facing ' + direction[currentDirIndex].dir);
        findHqPos();
    };

    var getDirectionIndex = function(turn) {
        var dir = currentDirIndex;

        turn = turn.toUpperCase();

        if(turn === 'R') {
            dir += 1;
            dir = (dir > direction.length-1) ? 0 : dir;
        } 
        else if(turn === 'L'){
            dir -= 1;
            dir = (dir < 0) ? direction.length-1 : dir;
        }

        return dir;
    };

    var findRepeatPos = function(targetPos) {
        return breadcrumb.filter(function (o) {
                return targetPos.x === o.x && targetPos.y === o.y;
            });
    };

    var findHqPos = function() {

        var repeatPos;

        // Loop through each breadcrum to find 1st repeated position
        while(breadcrumb.length > 0) {
            var targetPos = breadcrumb.shift();

            repeatPos = findRepeatPos(targetPos);

            if (repeatPos.length > 0) {
                var hqBlock = Math.abs(repeatPos[0].x) + Math.abs(repeatPos[0].y),
                    hqEl = document.querySelector('#hq');

                outputAnswer('[Day 1][Part 2] : Easter Bunny HQ is ' + hqBlock + ' blocks away');
                hqEl.style.left = (repeatPos[0].x * stepSize) + 'px';
                hqEl.style.top = (repeatPos[0].y * stepSize * -1) + 'px';
                break;
            }
        }
    };

    var outputAnswer = function(answer) {
        var answerList = document.querySelector('.answers'),
            el = document.createElement('li');

        el.innerHTML = answer;
        answerList.appendChild(el);
    };

    var createGrids = function() {
        var numCol = citySize / stepSize,
            rowsEl = document.querySelector('#rows'),
            colsEl = document.querySelector('#cols');

        for(var i=0; i < numCol; i++) {
            var rowBlockEl = document.createElement('div');
            rowBlockEl.classList.add('block', 'row');
            rowBlockEl.style.top = (stepSize * i) + 'px';
            rowsEl.appendChild(rowBlockEl);

            var colBlockEl = document.createElement('div');
            colBlockEl.classList.add('block', 'col');
            colBlockEl.style.left = (stepSize * i) + 'px';
            colsEl.appendChild(colBlockEl);
        }
    };

    var createRoute = function(steps) {
        var stepsInPx = steps * stepSize,
            prevPos = breadcrumb[breadcrumb.length - steps],
            routeContainer = document.querySelector('#route'),
            lineEl = document.createElement('div');

        lineEl.classList.add('line');

        var startPos, lineHeight, lineWidth;

        if(prevPos.x - pos.x !== 0) {
            lineWidth = stepsInPx;
            lineHeight = 1;

            if(prevPos.x < pos.x) {
                startPos = prevPos;
            } else {
                startPos = pos;
            }
        } else if(prevPos.y - pos.y !== 0) {
            lineWidth = 1;
            lineHeight = stepsInPx;

            if(prevPos.y > pos.y) {
                startPos = prevPos;
            } else {
                startPos = pos;
            }
        }
        
        lineEl.style.top = (startPos.y * stepSize * -1) + 'px';
        lineEl.style.left = (startPos.x * stepSize) + 'px';
        lineEl.style.width = lineWidth + 'px';
        lineEl.style.height = lineHeight + 'px';

        routeContainer.appendChild(lineEl);
    };

    return {
        Init : init
    };

}());