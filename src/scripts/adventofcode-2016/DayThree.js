
var log = require('bows')('App.DayThree');
var App = window.App || {};

App.DayThree = (function () {

    var inputData = [];

    var init = function () {
    	log('init');


        App.Utils.GetInputData('3.txt', true, onInputDataReady);
    };

    var onInputDataReady = function(data) {
    	var dataArr = data.split('\n');

    	dataArr.forEach(function(codeStr, i){
    		var codeArr = codeStr.split('');

    		inputData.push(codeArr);
    	});
    };

    var outputAnswer = function(answer) {
        var answerList = document.querySelector('.answers'),
            el = document.createElement('li');

        el.innerHTML = answer;
        answerList.appendChild(el);
    };

    return {
        Init : init
    };

}());
