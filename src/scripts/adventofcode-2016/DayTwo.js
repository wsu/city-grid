
var log = require('bows')('App.DayTwo');
var App = window.App || {};

App.DayTwo = (function () {

    var inputData = [],
    	keypadMatrix = [
	//	col: 0, 1, 2
    		[1, 2, 3], //row 0
    		[4, 5, 6], //row 1
    		[7, 8, 9]  //row 2
    	],
    	altKeypadMatrix = [
	//	col:  0,  1,  2,  3, 4
    		[-1, -1, 1, -1, -1], 	 //row 0
    		[-1, 2, 3, 4, -1], 		 //row 1
    		[5, 6, 7, 8, 9], 		 //row 2
    		[-1, 'A', 'B', 'C', -1], //row 3
    		[-1, -1, 'D', -1, -1],   //row 4
    	],
    	currentPos = {},
    	outputPass = [],
    	startKeyNum = 5;

    var init = function () {
    	log('init');


        App.Utils.GetInputData('2.txt', true, onInputDataReady);
    };

    var onInputDataReady = function(data) {
    	var dataArr = data.split('\n');

    	// Get starting position before decode
    	getInitPos(keypadMatrix);

    	dataArr.forEach(function(codeStr, i){
    		var codeArr = codeStr.split(''),
    			outputDigit = -1;

    		inputData.push(codeArr); // store it just in case

    		codeArr.forEach(function(code, j){
    			decode(code, keypadMatrix);
    		});

    		outputDigit = keypadMatrix[currentPos.row][currentPos.col];
    		outputPass.push(outputDigit);
    	});

        outputAnswer('[Day 2][Part 1] : Bathroom passcode is ' + outputPass.join(''));

    	// Lets find out answer for Part 2
    	altKeypadDecode();
    };

    var getInitPos = function(keypad) {
    	var initPos = {};

    	// Search #5's position in the specified keypad matrix
    	keypad.forEach(function(keypadRow, r){
    		var c = keypadRow.indexOf(startKeyNum);

	    	if(c !== -1) {
	    		initPos.col = c;
	    		initPos.row = r;
	    	}
    	});

    	currentPos.col = initPos.col;
    	currentPos.row = initPos.row;
    };

    var altKeypadDecode = function() {
    	var outputDigit = -1;

    	// Empty outputPass array
    	outputPass.length = 0;

    	// Get starting position before decode
    	getInitPos(altKeypadMatrix);

    	inputData.forEach(function(codeArr, j){
			codeArr.forEach(function(code, j){
    			decode(code, altKeypadMatrix);
    		});

			outputDigit = altKeypadMatrix[currentPos.row][currentPos.col];
			outputPass.push(outputDigit);
		});

        outputAnswer('[Day 2][Part 2] : Real bathroom passcode is ' + outputPass.join(''));
    };

    var decode = function(code, keypad) {
    	var newPos = {row : currentPos.row, col : currentPos.col};

    	switch(code) {
    		case 'U':
    			newPos.row--;
    			newPos.row = (newPos.row < 0) ? 0 : newPos.row;
    		break;
    		case 'D':
    			newPos.row++;
    			newPos.row = (newPos.row > keypad.length-1) ? keypad.length-1 : newPos.row;
    		break;
    		case 'L':
    			newPos.col--;
    			newPos.col = (newPos.col < 0) ? 0 : newPos.col;
    		break;
    		case 'R':
    			newPos.col++;
    			newPos.col = (newPos.col > keypad[newPos.row].length-1) ? keypad[newPos.row].length-1 : newPos.col;
    		break;
    	}

    	// if new position lands on -1 of the keypad, it is an invalid position, hence don't move
		if(keypad[newPos.row][newPos.col] !== -1) {
			currentPos.row = newPos.row;
	    	currentPos.col = newPos.col;
	    }
    };

    var outputAnswer = function(answer) {
        var answerList = document.querySelector('.answers'),
            el = document.createElement('li');

        el.innerHTML = answer;
        answerList.appendChild(el);
    };

    return {
        Init : init
    };

}());