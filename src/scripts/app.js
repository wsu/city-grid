
 // NOTE: Run "localStorage.debug=true" to have bows showing up in console.
var log = require('bows')('App');

var App = App || {};
window.App = App;

var Config = require('./Config');
var Utils = require('./Utils');
var DayOne = require('./adventofcode-2016/DayOne');
var DayTwo = require('./adventofcode-2016/DayTwo');
var DayThree = require('./adventofcode-2016/DayThree');

// App Initialization
// --------------------------------------------------
(function () {

	App.DayOne.Init();
	App.DayTwo.Init();
	App.DayThree.Init();

}());